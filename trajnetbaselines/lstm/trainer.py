"""Command line tool to train an LSTM model."""

# todo: clean the input section (remove unnecessary args and add new ones)

from datetime import datetime

import argparse
from csv import writer
import logging
import socket
import math
import sys
import time
import random
import os
import pickle
import torch
import numpy as np
from tqdm import tqdm
from torch.autograd import Variable 
from torch import nn

import trajnetplusplustools

from .. import augmentation

# added for attack model
from ..attack_modules.attcker import Attacker
from ..attack_modules.detector import Detector
from ..attack_modules.discriminator import Discriminator
from ..attack_modules.loss import AttackerLoss
from ..attack_modules.loss import DetectorLoss
from ..attack_modules.loss import DiscriminatorLoss

from .loss import PredictionLoss, L2Loss
from .lstm import LSTM, LSTMPredictor, drop_distant
from .gridbased_pooling import GridBasedPooling
from .non_gridbased_pooling import NN_Pooling, HiddenStateMLPPooling, AttentionMLPPooling, DirectionalMLPPooling
from .non_gridbased_pooling import NN_LSTM, TrajectronPooling, SAttention, SAttention_fast
from .more_non_gridbased_pooling import NMMP

from .. import __version__ as VERSION

from .utils import center_scene, random_rotation


def save_model(epoch, model, loss, PATH, model_name):
    torch.save(model.state_dict(), PATH + model_name + '.epoch.' + str(epoch))


def save_log(str, address):
  f = open(address, 'a+')
  f.write(str + '\n')
  f.close()

def append_list_as_row(file_name, list_of_elem): # appends a row to a csv file
    with open(file_name, 'a+', newline='') as write_obj:
        csv_writer = writer(write_obj)
        csv_writer.writerow(list_of_elem)

def clamp(vector, barrier):
    n = torch.norm(vector.view(-1, 2), 2, dim=1)
    for i in range(len(n)):
      if n[i] > barrier:
        vector[2 * i] *= barrier / n[i]
        vector[2 * i + 1] *= barrier/ n[i]
    return vector


def save_tensor_to_csv(filename, x): # saves a tensor to a csv with name: filename in the main folder
    l = x.tolist()
    num_frames = len(l)
    num_agents = len(l[0])


    if not os.path.exists(os.path.dirname(filename)):
        try:# Attacker stuff

            os.makedirs(os.path.dirname(filename))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    file1 = open(filename, 'w')
    s = '{"scene": {"id": 0, "p": 0, "s": 1, "e":' +str(num_frames) + ', "fps": 2.5, "tag": 0}}\n'
    file1.write(s)
    bad = False
    for agent in range(num_agents):
        prv_x, prv_y = -1, -1
        for frame in range(0, num_frames, 1):
            x = l[frame][agent][0]
            y = l[frame][agent][1]

            if math.isnan(x) or math.isnan(y):
                l[frame][agent][0] = prv_x
                l[frame][agent][1] = prv_y
                bad = True
            else:
                prv_x = x
                prv_y = y
    for agent in range(num_agents):
        prv_x, prv_y = -1, -1
        for frame in range(num_frames - 1, -1, -1):
            x = l[frame][agent][0]
            y = l[frame][agent][1]

            if x == -1 or y == -1:
                l[frame][agent][0] = prv_x
                l[frame][agent][1] = prv_y
                bad = True
            else:
                prv_x = x
                prv_y = y
    show_one_agent = True
    for agent in range(num_agents):
        for frame in range(num_frames):
            if show_one_agent == True and agent != 0:
                continue
            x = l[frame][agent][0]
            y = l[frame][agent][1]
            x = round(x, 2)
            y = round(y, 2)
            s = '{"track": {"f": ' + str(frame + 1) + ", " +  '"p": ' + str(agent + 1) +  ', "x": ' + str(x) + ', ' + '"y": ' + str(y) + '}}\n'
            file1.write(s)
            append_list_as_row(filename, [frame + 1, agent + 1, round(x, 2), round(y, 2)])
    #print("saved ", filename, " with scene info.")
    return bad


def calc_fde_ade(output, ground_truth): # input: two tensors, returns fde, ade
    l = output.tolist()
    l2 = ground_truth.tolist()
    num_frames_output = len(l)
    num_frames_truth = len(l2)
    delta = num_frames_output - num_frames_truth
    distances = []
    for frame in range(num_frames_output):
        if frame + num_frames_truth >= num_frames_output:
            x1 = l[frame][0][0]  # for agent 0
            y1 = l[frame][0][1]
            x2 = l2[frame - delta][0][0]
            y2 = l2[frame - delta][0][1]
            d = np.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2))
            distances.append(d)
    #print(distances)
    return distances[-1], np.mean(distances)


def pointwise_perturbation(output, ground_truth): # input: two tensors, returns fde, ade
    l = output.tolist()
    l2 = ground_truth.tolist()
    num_frames_output = len(l)
    num_frames_truth = len(l2)
    delta = num_frames_output - num_frames_truth
    distances = []
    for frame in range(num_frames_output):
        if frame + num_frames_truth >= num_frames_output:
            x1 = l[frame][0][0]  # for agent 0
            y1 = l[frame][0][1]
            x2 = l2[frame - delta][0][0]
            y2 = l2[frame - delta][0][1]
            d = np.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2))
            distances.append(d)
    #print(distances)
    return distances


class Trainer(object):
    def __init__(self, model=None, criterion='L2', lr=None, barrier = 1,
                 device=None, batch_size=32, obs_length=9, pred_length=12, augment=False,
                 normalize_scene=False, save_every=1, start_length=0, obs_dropout=False,
                 log_file_address = 'trajnetbaselines/lstm/Attack-Models/Logs',
                 detector_loss_weight=5, discriminator_loss_weight=1,
                 pre_trained = False, attacker_model_name = 'attacker.state', discriminator_model_name = 'discriminator.state'):
        
        now = datetime.now()
        date_time = now.strftime("%m.%d.%Y, %H:%M:%S")

        self.log_file_address_train = log_file_address + '/log_train_' + str(date_time) + '.txt'

        self.log_file_address_val = log_file_address + '/log_val_' + str(date_time) + '.txt'

        self.model = model if model is not None else LSTM()

        self.device = device if device is not None else torch.device('cpu')

        self.model = self.model.to(self.device)

        # parameters
        self.lr = lr
        self.show_limit = 10 # number of samples to draw
        self.barrier = barrier
        self.base_dir = 'trajnetplusplustools/scenes/'

        self.log = logging.getLogger(self.__class__.__name__)
        self.save_every = save_every

        self.batch_size = batch_size
        self.obs_length = obs_length
        self.pred_length = pred_length
        self.seq_length = self.obs_length + self.pred_length

        self.augment = augment
        self.normalize_scene = normalize_scene

        self.start_length = start_length
        self.obs_dropout = obs_dropout

        self.all_fde = {}
        self.all_ade = {}
        self.all_fde['normal'] = []
        self.all_fde['perturb'] = []
        self.all_ade['normal'] = []
        self.all_ade['perturb'] = []
        self.all_ade['delta'] = []
        self.all_fde['delta'] = []
        self.points_perturbation = []
        self.results_log = ''
    def add_to_log(self, new_line):
        print(new_line)
        self.results_log = self.results_log + new_line + '\n'
    def numerical_stats(self): # prints numerical stats
        self.add_to_log("FDE on observed trajectories of observations in the test set")
        self.add_to_log(str(round(np.mean(self.all_fde['normal']), 4) ) )

        self.add_to_log("ADE on observed trajectories of observations in the test set")
        self.add_to_log(str(round(np.mean(self.all_ade['normal']), 4)) )

        self.add_to_log("FDE on predicted trajectories of perturbed observations")
        self.add_to_log(str(round(np.mean(self.all_fde['perturb']), 4) ) )

        self.add_to_log("ADE on predicted trajectories of perturbed observations")
        self.add_to_log(str(round(np.mean(self.all_ade['perturb']), 4)) )

        self.add_to_log("FDE of difference")
        self.add_to_log(str(round(np.mean(self.all_fde['delta']), 4)) )

        self.add_to_log("ADE on difference")
        self.add_to_log(str(round(np.mean(self.all_ade['delta']), 4)) )

    def loop(self, train_scenes, val_scenes, train_goals, val_goals, out):
        models_path = 'trajnetbaselines/lstm/Attack-Models'
        self.train(train_scenes, train_goals, 1)

    def train(self, scenes, goals, epoch):
        start_time = time.time()
        batch_counter = 0

        print('epoch', epoch)

        # random.shuffle(scenes)
        print("TOTAL scenes: ", len(scenes))
        for scene_i, (filename, scene_id, paths) in tqdm(enumerate(scenes)):
            scene_start = time.time()

            ## make new scene
            scene = trajnetplusplustools.Reader.paths_to_xy(paths)

            ## get goals
            if goals is not None:
                scene_goal = np.array(goals[filename][scene_id])
            else:
                scene_goal = np.array([[0, 0] for path in paths])

            ## Drop Distant
            scene, mask = drop_distant(scene)
            scene_goal = scene_goal[mask]

            ##process scene
            if self.normalize_scene:
                scene, _, _, scene_goal = center_scene(scene, self.obs_length, goals=scene_goal)
            if self.augment:
                scene, scene_goal = random_rotation(scene, goals=scene_goal)
                # scene = augmentation.add_noise(scene, thresh=0.01)

            scene = torch.Tensor(scene).to(self.device)
            scene_goal = torch.Tensor(scene_goal).to(self.device)
            preprocess_time = time.time() - scene_start

            a, b, c = self.train_batch(scene, scene_goal)
            # for testing log_file making
            if a == -1 and b == -1 and c == -1:
                break
            total_time = time.time() - scene_start


    def train_batch(self, xy, goals):

        observed = xy[self.start_length:self.obs_length].clone()
        prediction_truth: object = xy[self.obs_length:self.seq_length - 1].clone()  ## CLONE
        targets = xy[self.obs_length:self.seq_length] - xy[self.obs_length - 1:self.seq_length - 1]

        # PGD
        target_agent_observed_path = observed[:, 0].reshape(-1)
        temp = [0.001 for _ in range(len(target_agent_observed_path) - 4)]
        x = np.array(temp)
        noise = torch.tensor(x, requires_grad=True)

        optimizer = torch.optim.Adam([noise], lr=self.lr)

        rel_outputs, outputs = self.model(observed.clone(), goals.clone(), prediction_truth.clone())
        num_agents = len(outputs.tolist()[0])
        
        best_score_by_now = -1
        best_observation_by_now = observed.clone()
        counter = 30

        while (counter != 0):
            perturbed_path = target_agent_observed_path.clone().data
            perturbed_path[2:-2] += noise
            
            perturbed_observation = observed.clone()
            perturbed_observation[:self.obs_length, 0] = perturbed_path.view(-1, 2)

            temp_perturbed_observation = perturbed_observation.clone()

            rel_outputs_perturbed, outputs_perturbed = self.model(perturbed_observation, goals.clone(),
                                                                  prediction_truth.clone())

            loss = -torch.sum(
                torch.norm(outputs_perturbed[-self.pred_length:, 0] - outputs[-self.pred_length:, 0].data, 2, dim=1))
                
            score = -loss
            if score > best_score_by_now:
                counter = 30
                best_score_by_now = score
                best_observation_by_now = temp_perturbed_observation.clone()
            else:
                counter -= 1
                
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            noise.data = clamp(noise.data, self.barrier)
        
        perturbed_observation = best_observation_by_now.clone()

        
        # Model stuff
        rel_outputs, outputs = self.model(observed.clone(), goals.clone(), prediction_truth.clone())

        rel_outputs_perturbed, outputs_perturbed = self.model(perturbed_observation.clone(), goals.clone(),
                                                              prediction_truth.clone())

        fde, ade = calc_fde_ade(output=perturbed_observation, ground_truth=observed)
        self.all_ade['normal'].append(ade)
        self.all_fde['normal'].append(fde)

        # for outputs after perturbing
        fde, ade = calc_fde_ade(output=outputs_perturbed, ground_truth=prediction_truth)
        self.all_ade['perturb'].append(ade)
        self.all_fde['perturb'].append(fde)

        # for delta of outputs and perturb
        fde, ade = calc_fde_ade(output=outputs_perturbed[-self.pred_length:], ground_truth=outputs[-self.pred_length:])
        self.all_ade['delta'].append(ade)
        self.all_fde['delta'].append(fde)



        if self.show_limit >= 1 and num_agents >= 2:
            self.show_limit = self.show_limit - 1
            res1 = save_tensor_to_csv(
                self.base_dir + '/outputs' + str(self.show_limit) + '.ndjson',
                torch.cat((observed[: self.obs_length], outputs[-self.pred_length:])))
            res2 = save_tensor_to_csv(self.base_dir + '/outputs_perturbed' + str(
                self.show_limit) + '.ndjson', torch.cat(
                (perturbed_observation[: self.obs_length], outputs_perturbed[-self.pred_length:])))

        ## Loss wrt primary only

        return 0, 0, 0



def prepare_data(path, subset='/train/', sample=1.0, goals=True):
    """ Prepares the train/val scenes and corresponding goals """

    ## read goal files
    all_goals = {}
    all_scenes = []

    ## List file names
    files = [f.split('.')[-2] for f in os.listdir(path + subset) if f.endswith('.ndjson')]
    ## Iterate over file names
    for file in files:
        reader = trajnetplusplustools.Reader(path + subset + file + '.ndjson', scene_type='paths')
        ## Necessary modification of train scene to add filename
        scene = [(file, s_id, s) for s_id, s in reader.scenes(sample=sample)]
        if goals:
            goal_dict = pickle.load(open('dest_new/' + subset + file + '.pkl', "rb"))
            ## Get goals corresponding to train scene
            all_goals[file] = {s_id: [goal_dict[path[0].pedestrian] for path in s] for _, s_id, s in scene}
        all_scenes += scene

    if goals:
        return all_scenes, all_goals
    return all_scenes, None


def main(epochs=10):
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', default=epochs, type=int,
                        help='number of epochs')
    parser.add_argument('--barrier', default=1, type=float,
                        help='barrier for noise')
    parser.add_argument('--step_size', default=15, type=int,
                        help='step_size of scheduler')
    parser.add_argument('--save_every', default=1, type=int,
                        help='frequency of saving model')
    parser.add_argument('--obs_length', default=9, type=int,
                        help='observation length')
    parser.add_argument('--pred_length', default=12, type=int,
                        help='prediction length')
    parser.add_argument('--batch_size', default=1, type=int,
                        help='number of epochs')
    parser.add_argument('--lr', default=1e-3, type=float,
                        help='initial learning rate')
    parser.add_argument('--type', default='social',
                        choices=('vanilla', 'occupancy', 'directional', 'social', 'hiddenstatemlp', 's_att_fast',
                                 'directionalmlp', 'nn', 'attentionmlp', 'nn_lstm', 'traj_pool', 's_att', 'nn_tag',
                                 'nmmp'),
                        help='type of LSTM to train')
    parser.add_argument('--norm_pool', action='store_true',
                        help='normalize_pool (along direction of movement)')
    parser.add_argument('--front', action='store_true',
                        help='Front pooling (only consider pedestrian in front along direction of movement)')
    parser.add_argument('-o', '--output', default=None,
                        help='output file')
    parser.add_argument('--disable-cuda', action='store_true',
                        help='disable CUDA')
    parser.add_argument('--augment', action='store_true',
                        help='augment scenes')
    parser.add_argument('--normalize_scene', action='store_true',
                        help='augment scenes')
    parser.add_argument('--path', default='trajdata',
                        help='glob expression for data files')
    parser.add_argument('--goal_path', default=None,
                        help='glob expression for goal files')
    parser.add_argument('--loss', default='L2',
                        help='loss function')
    parser.add_argument('--goals', action='store_true',
                        help='to use goals')

    pretrain = parser.add_argument_group('pretraining')
    pretrain.add_argument('--load-state', default=None,
                          help='load a pickled model state dictionary before training')
    pretrain.add_argument('--load-full-state', default=None,
                          help='load a pickled full state dictionary before training')
    pretrain.add_argument('--nonstrict-load-state', default=None,
                          help='load a pickled state dictionary before training')

    ##Pretrain Pooling AE
    pretrain.add_argument('--load_pretrained_pool_path', default=None,
                          help='load a pickled model state dictionary of pool AE before training')
    pretrain.add_argument('--pretrained_pool_arch', default='onelayer',
                          help='architecture of pool representation')
    pretrain.add_argument('--downscale', type=int, default=4,
                          help='downscale factor of pooling grid')
    pretrain.add_argument('--finetune', type=int, default=0,
                          help='finetune factor of pretrained model')

    hyperparameters = parser.add_argument_group('hyperparameters')
    hyperparameters.add_argument('--hidden-dim', type=int, default=128,
                                 help='RNN hidden dimension')
    hyperparameters.add_argument('--coordinate-embedding-dim', type=int, default=64,
                                 help='coordinate embedding dimension')
    hyperparameters.add_argument('--cell_side', type=float, default=0.6,
                                 help='cell size of real world')
    hyperparameters.add_argument('--n', type=int, default=16,
                                 help='number of cells per side')
    hyperparameters.add_argument('--layer_dims', type=int, nargs='*',
                                 help='interaction module layer dims for gridbased pooling')
    hyperparameters.add_argument('--pool_dim', type=int, default=256,
                                 help='pooling dimension')
    hyperparameters.add_argument('--embedding_arch', default='two_layer',
                                 help='interaction arch')
    hyperparameters.add_argument('--goal_dim', type=int, default=64,
                                 help='goal dimension')
    hyperparameters.add_argument('--spatial_dim', type=int, default=32,
                                 help='attention mlp spatial dimension')
    hyperparameters.add_argument('--vel_dim', type=int, default=32,
                                 help='attention mlp vel dimension')
    hyperparameters.add_argument('--pool_constant', default=0, type=int,
                                 help='background of pooling grid')
    hyperparameters.add_argument('--sample', default=1.0, type=float,
                                 help='sample ratio of train/val scenes')
    hyperparameters.add_argument('--norm', default=0, type=int,
                                 help='normalization scheme for grid-based')
    hyperparameters.add_argument('--no_vel', action='store_true',
                                 help='dont consider velocity in nn')
    hyperparameters.add_argument('--neigh', default=4, type=int,
                                 help='neighbours to consider in DirectConcat')
    hyperparameters.add_argument('--mp_iters', default=5, type=int,
                                 help='message passing iters in NMMP')
    hyperparameters.add_argument('--start_length', default=0, type=int,
                                 help='prediction length')
    hyperparameters.add_argument('--obs_dropout', action='store_true',
                                 help='obs length dropout')
    args = parser.parse_args()

    #todo: Add Items to Args
    discriminator_type = 1

    if args.sample < 1.0:
        torch.manual_seed("080819")
        random.seed(1)

    if not os.path.exists('OUTPUT_BLOCK/{}'.format(args.path)):
        os.makedirs('OUTPUT_BLOCK/{}'.format(args.path))
    if args.goals:
        args.output = 'OUTPUT_BLOCK/{}/lstm_goals_{}_{}.pkl'.format(args.path, args.type, args.output)
    else:
        args.output = 'OUTPUT_BLOCK/{}/lstm_{}_{}.pkl'.format(args.path, args.type, args.output)

    # configure logging
    from pythonjsonlogger import jsonlogger
    print(args.load_full_state)
    if args.load_full_state:
        file_handler = logging.FileHandler(args.output + '.log', mode='a')
    else:
        file_handler = logging.FileHandler(args.output + '.log', mode='w')
    file_handler.setFormatter(jsonlogger.JsonFormatter('(message) (levelname) (name) (asctime)'))
    stdout_handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, handlers=[stdout_handler, file_handler])
    logging.info({
        'type': 'process',
        'argv': sys.argv,
        'args': vars(args),
        'version': VERSION,
        'hostname': socket.gethostname(),
    })

    # refactor args for --load-state
    args.load_state_strict = True
    if args.nonstrict_load_state:
        args.load_state = args.nonstrict_load_state
        args.load_state_strict = False
    if args.load_full_state:
        args.load_state = args.load_full_state

    # add args.device
    args.device = torch.device('cpu')
    # if not args.disable_cuda and torch.cuda.is_available():
    #     args.device = torch.device('cuda')

    args.path = 'DATA_BLOCK/' + args.path
    ## Prepare data


    test_path = 'DATA_BLOCK/collision_test'
    test_scenes, test_goals = prepare_data(test_path, subset='/test_private/', sample=args.sample, goals=args.goals)
    val_scenes, val_goals = prepare_data(args.path, subset='/val/', sample=args.sample, goals=args.goals)

    ## pretrained pool model (if any)
    pretrained_pool = None

    # create model (Various interaction/pooling modules)
    pool = None
    if args.type == 'hiddenstatemlp':
        pool = HiddenStateMLPPooling(hidden_dim=args.hidden_dim, out_dim=args.pool_dim,
                                     mlp_dim_vel=args.vel_dim)
    elif args.type == 'nmmp':
        pool = NMMP(hidden_dim=args.hidden_dim, out_dim=args.pool_dim, k=args.mp_iters)
    elif args.type == 'attentionmlp':
        pool = AttentionMLPPooling(hidden_dim=args.hidden_dim, out_dim=args.pool_dim,
                                   mlp_dim_spatial=args.spatial_dim, mlp_dim_vel=args.vel_dim)
    elif args.type == 'directionalmlp':
        pool = DirectionalMLPPooling(out_dim=args.pool_dim)
    elif args.type == 'nn':
        pool = NN_Pooling(n=args.neigh, out_dim=args.pool_dim, no_vel=args.no_vel)
    elif args.type == 'nn_lstm':
        pool = NN_LSTM(n=args.neigh, hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type == 'traj_pool':
        pool = TrajectronPooling(hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type == 's_att':
        pool = SAttention(hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type == 's_att_fast':
        pool = SAttention_fast(hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type != 'vanilla':
        pool = GridBasedPooling(type_=args.type, hidden_dim=args.hidden_dim,
                                cell_side=args.cell_side, n=args.n, front=args.front,
                                out_dim=args.pool_dim, embedding_arch=args.embedding_arch,
                                constant=args.pool_constant, pretrained_pool_encoder=pretrained_pool,
                                norm=args.norm, layer_dims=args.layer_dims)

    model = LSTM(pool=pool,
                 embedding_dim=args.coordinate_embedding_dim,
                 hidden_dim=args.hidden_dim,
                 goal_flag=args.goals,
                 goal_dim=args.goal_dim)

    # train
    # loads here ->
    load_address = 'trajnetbaselines/lstm/Target-Model/'
    models_name = os.listdir(load_address)
    load_address = load_address + models_name[0]

    # load pretrained model.
    # useful for transfer learning
    print("Loading Model Dict")
    with open(load_address, 'rb') as f:
        checkpoint = torch.load(f)
    pretrained_state_dict = checkpoint['state_dict']
    print("Successfully Loaded")
    # ?
    model.load_state_dict(pretrained_state_dict, strict=False)

    # Freeze the model
    for p in model.parameters():
        p.requires_grad = False

    # trainer
    # todo: add new args

    trainer = Trainer(model, lr=args.lr, device=args.device, barrier=args.barrier,
                      criterion=args.loss, batch_size=args.batch_size, obs_length=args.obs_length,
                      pred_length=args.pred_length, augment=args.augment, normalize_scene=args.normalize_scene,
                      save_every=args.save_every, start_length=args.start_length, obs_dropout=args.obs_dropout)
    # go through all test dataset
    trainer.loop(test_scenes, val_scenes, test_goals, val_goals, args.output)
    trainer.numerical_stats()
    all_ade_fde = trainer.results_log
    text_file = open("numerical_log_pgd.txt", "w")
    text_file.write(all_ade_fde)
    text_file.close()

if __name__ == '__main__':
    main()

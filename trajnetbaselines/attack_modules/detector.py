import torch
from torch import nn


class Detector(nn.Module):
    def __init__(self):
        super(Detector, self).__init__()

    def forward(self, model_prediction, ground_truth):
        norms = torch.norm(model_prediction - ground_truth, 2, dim=1)
        return torch.sum(norms)

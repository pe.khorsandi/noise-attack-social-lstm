python -m trajnetbaselines.lstm.visualize \
--batch_size 1 \
--lr 1e-4 \
--epochs 1 \
--layer_dims 512
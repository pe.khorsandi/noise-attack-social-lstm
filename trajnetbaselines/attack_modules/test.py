import torch

x = torch.Tensor([[i for i in range(1, 3)] for _ in range(3)])
y = torch.Tensor([[i * j for i in range(2)] for j in range(3)])
print(x - y)
print(torch.sum(torch.norm(x - y, 2, dim=1)))

python -m trajnetbaselines.lstm.trainer \
--batch_size 60 \
--lr 0.005 \
--epochs 70 \
--layer_dims 1024 \
--barrier 0.1
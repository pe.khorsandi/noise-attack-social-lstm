import torch
from torch import nn


class Attacker(nn.Module):
    def __init__(self, input_size=2*9, output_size=2*9):
        super(Attacker, self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_size, 16),
            nn.ReLU(True), nn.Linear(16, 8))
        self.decoder = nn.Sequential(
            nn.Linear(8, 16),
            nn.ReLU(True), nn.Linear(16, output_size), nn.Tanh())

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

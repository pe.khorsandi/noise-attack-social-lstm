import os
num_samples = 10


for sample in range(num_samples):
    print(sample)
    normal = 'python -m trajnetplusplustools.trajectories \\\n--real scenes/outputs' + str(sample) + '.ndjson \\'  + '\n--perturbed scenes/outputs_perturbed' + str(sample) + '.ndjson'
    os.system(normal)


import torch
from torch import nn


class Discriminator(nn.Module):
    def __init__(self, barrier, barrier_value):
        super(Discriminator, self).__init__()
        self.barrier = torch.Tensor([barrier])
        self.barrier_value = torch.Tensor([barrier_value])

    def forward(self, x, ground_truth):
        noise = torch.abs(x - ground_truth)
        loss = torch.Tensor([0])
        for t in (self.barrier.repeat(len(noise)) - noise):
            if t > 0:
              loss += -torch.log(t) / self.barrier_value
            else:
              loss += 1000 * t * t
        return loss

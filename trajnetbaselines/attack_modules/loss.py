import torch
from torch import nn
from torch.autograd import Variable 


# The attacker loss is a weighted combination of two separated losses
class AttackerLoss(nn.Module):
    def __init__(self, detector_weight=1, discriminator_weight=1):
        super(AttackerLoss, self).__init__()
        self.detector_loss = DetectorLoss()
        self.discriminator_loss = DiscriminatorLoss()
        self.discriminator_weight = discriminator_weight
        self.detector_weight = detector_weight

    def forward(self, detector_output, discriminator_output):
        discriminiator_FL = Variable(-(discriminator_output) * torch.log(1 - discriminator_output), requires_grad=False)
        detector_key = (self.detector_loss(detector_output) - torch.Tensor([0.3]))/torch.Tensor([0.6])
        if detector_key < 0:
          detector_key = torch.Tensor([100])
        detector_FL = Variable(-(1-detector_key)*torch.log(detector_key), requires_grad=False)

        return - self.detector_weight *  self.detector_loss(detector_output) \
              + self.discriminator_weight * self.discriminator_loss(discriminator_output)


class DiscriminatorLoss(nn.Module):
    def __init__(self):
        super(DiscriminatorLoss, self).__init__()

    def forward(self, discriminator_output):
        return discriminator_output


class DetectorLoss(nn.Module):
    def __init__(self):
        super(DetectorLoss, self).__init__()

    def forward(self, detector_output):
        return torch.tanh(detector_output/40)
